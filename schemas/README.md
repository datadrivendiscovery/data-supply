# D3M Schemas

**version# 4.1.1**

Please see the contents of [documentation/](../documentation/) for details.

Schemas are described using [Cerberus](https://docs.python-cerberus.org/en/stable/).
