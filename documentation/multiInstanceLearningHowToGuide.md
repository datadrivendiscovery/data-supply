# Preparation of Multiple Instance Learning Datasets for D3M

Here we illustrate the key steps involved in preparing a dataset for multiple instance (multi-instance) learning (MIL) problem for D3M systems. Instead of receiving a set of instances which are individually labeled, in multi-instance learning the learner receives a set of labeled bags, each containing many instances. In the simple case of multi-instance binary classification, a bag may be labeled negative if all the instances in it are negative. On the other hand, a bag is labeled positive if there is at least one instance in it which is positive. From a collection of labeled bags, the learner has to either induce a concept that will label individual instances correctly, or learn how to label bags without inducing the concept. We will illustrate the dataset curation process for this kind of problems using the example of the commonly benchmarked Musk data [<sup>(Dietterich et al., 1997)</sup>](#ref1). But these steps are generalizable to other multi-instance datasets. 

Consider the following data which captures a portion of the Musk data.

| instanceID | bagID | bagLabel | feature_3 | feature_4 | feature_5 | feature_6 | … | … | feature_167 | feature_168 |
|:----------:|:-----:|:--------:|:---------:|:---------:|:---------:|:---------:|:-:|:-:|:-----------:|:-----------:|
|  f159_1+1  |  B_35 |     1    |     55    |    -138   |    -59    |    -74    | … | … |     -19     |      38     |
|  f159_1+2  |  B_35 |     1    |     57    |    -81    |    -75    |    -64    | … | … |     -22     |      34     |
|   288_1+1  |  B_59 |     0    |     49    |    -132   |    -93    |     30    | … | … |     -14     |     -10     |
|   288_1+2  |  B_59 |     0    |     51    |    -119   |    -24    |    -39    | … | … |      10     |      96     |
|      …     |   …   |     …    |     …     |     …     |     …     |     …     | … | … |      …      |      …      |
|      …     |   …   |     …    |     …     |     …     |     …     |     …     | … | … |      …      |      …      |
|   j97_1+1  |  B_89 |     0    |     22    |     69    |     29    |     39    | … | … |     -147    |      62     |
|   j97_1+2  |  B_89 |     0    |     19    |     69    |     36    |     40    | … | … |     -142    |      79     |
|      …     |   …   |     …    |     …     |     …     |     …     |     …     | … | … |      …      |      …      |
|      …     |   …   |     …    |     …     |     …     |     …     |     …     | … | … |      …      |      …      |
|   232_4+2  |  B_52 |     0    |     43    |    -102   |    -19    |    -98    | … | … |     -20     |     -38     |
|   253_1+1  |  B_55 |     0    |     42    |    -188   |    -126   |    -109   | … | … |      16     |      27     |
|   253_1+2  |  B_55 |     0    |     42    |    -199   |    -164   |    -46    | … | … |      13     |      26     |

MIL data is characterized by the presence of an instance ID and a bag ID with each row. There is a one-to-many relationship between the bags and instances as shown above. The following are the key steps to preparing such a MIL dataset in D3M:

## Key steps:
1. [Create directory structure](#step1)
2. [Save the data table as a CSV file](#step2)
3. [Create a dataset schema](#step3)
4. [Create a problem schema](#step4)

<a name="step1"></a>
## Step 1: Create directory structure
As a first step, create the following directory structure.

```
Musk
└── Musk_dataset
    └── tables
```

<a name="step2"></a>
## Step 2: Save the data table as a CSV file

Add a special `d3mIndex` column to the original table and populate it with row IDs starting with 0 as shown below.

| d3mIndex | instanceID | bagID | bagLabel | … | … | feature_168 |
|:--------:|:----------:|:-----:|:--------:|:-:|:-:|:-----------:|
|     0    |  f159_1+1  |  B_35 |     1    | … | … |      38     |
|     1    |  f159_1+2  |  B_35 |     1    | … | … |      34     |
|     2    |   288_1+1  |  B_59 |     0    | … | … |     -10     |
|     3    |   288_1+2  |  B_59 |     0    | … | … |      96     |
|     …    |      …     |   …   |     …    | … | … |      …      |
|     …    |      …     |   …   |     …    | … | … |      …      |
|    34    |   j97_1+1  |  B_89 |     0    | … | … |      62     |
|    35    |   j97_1+2  |  B_89 |     0    | … | … |      79     |
|     …    |      …     |   …   |     …    | … | … |      …      |
|     …    |      …     |   …   |     …    | … | … |      …      |
|    473   |   232_4+2  |  B_52 |     0    | … | … |     -38     |
|    474   |   253_1+1  |  B_55 |     0    | … | … |      27     |
|    475   |   253_1+2  |  B_55 |     0    | … | … |      26     |

Save the table in a CSV file called `learningData.csv` under the `tables` directory as shown below.

```
Musk
└── Musk_dataset
    └── tables
        └── learningData.csv
```

<a name="step3"></a>
## Step 3: Create a dataset schema file

In addition to the CSV file, D3M datasets require a dataset schema (JSON) file. Create a file called `datasetDoc.json` and save it under `Musk_dataset` directory as shown below.
```
Musk
└── Musk_dataset
    └── tables
    │    └── learningData.csv
    └── datasetDoc.json
```

This JSON file should contain metadata about your dataset in a machine readable form and comply with the official [D3M dataset schema definition](https://gitlab.com/datadrivendiscovery/data-supply/-/blob/shared/documentation/datasetSchema.md). Refer to this manual for details regarding the required fields, their semantics, and their expected range of values.

But the easiest way to get started is to use the following JSON structure and customize it to reflect your dataset and the columns in your data. Then save it in the `datasetDoc.json` file. Notice that this schema captures some basic information about the dataset such as it's ID, name, and versions. It also captures additional information about the table such as column names, their data types, and roles. 

```
{
  "about": {
    "datasetID": "Musk_dataset",
    "datasetName": "MIL MUSK dataset",
    "description": "This is a seed multiple instance learning dataset.",
    "datasetSchemaVersion": "4.0.0",
    "datasetVersion": "1.0.0"
  },
  "dataResources": [
    {
      "resID": "learningData",
      "resPath": "tables/learningData.csv",
      "resType": "table",
      "resFormat": {
        "text/csv": [
          "csv"
        ]
      },
      "isCollection": false,
      "columns": [
        {
          "colIndex": 0,
          "colName": "d3mIndex",
          "colType": "integer",
          "role": [
            "index"
          ]
        },
        {
          "colIndex": 1,
          "colName": "instanceID",
          "colType": "categorical",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 2,
          "colName": "bagID",
          "colType": "categorical",
          "role": [
            "attribute",
            "bagKey"
          ]
        },
        {
          "colIndex": 3,
          "colName": "bagLabel",
          "colType": "categorical",
          "role": [
            "suggestedTarget"
          ]
        },
        {
          "colIndex": 4,
          "colName": "feature_3",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 5,
          "colName": "feature_4",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        ...
        ...
        {
          "colIndex": 169,
          "colName": "feature_168",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        }
      ]
    }
  ]
}
```
Note that `bagID` column has a `bagKey` role in addition to being an attribute. This indicates that this column holds the bag IDs. Also note that it is not required to specify the column metadata for all the columns. Where this metadata is missing, this information is inferred by D3M systems where possible.

<a name="step4"></a>
## Step 4: Create a problem schema

To specify a forecasting problem over this data, create a new directory called `Musk_problem` under the project root as shown below. Then create a problem schema and save it in a JSON file called `problemDoc.json` under the `Musk_problem` directory as shown below.

```
Musk
└── Musk_dataset
│   └── tables
│   │    └── learningData.csv
│   └── datasetDoc.json
└── Musk_problem
    └── problemDoc.json
```

The full problem schema definition can be found in the official [D3M problem schema definition](https://gitlab.com/datadrivendiscovery/data-supply/-/blob/shared/documentation/problemSchema.md) document. Refer to this manual for details regarding the contents of this `problemDoc.json` file.

But the easiest way to get started is to use the following JSON structure and customize it to reflect your problem and save it in the `problemDoc.json` file. Notice that this schema captures some basic information about the problem such as it's ID, name, and versions. It also captures information about the task (`binary` `classification` qualified by `multipleInstanceLearning`) and has a reference to the dataset part along with some information about the target column. Finally, it also captures information about the performance metric (accuracy, in this case).

```
{
  "about": {
    "problemID": "Musk_problem",
    "problemName": "MIL MUSK problem",
    "problemDescription": "This is a sample multiple instance learning problem",
    "problemSchemaVersion": "4.0.0",
    "problemVersion": "1.0.0",
    "taskKeywords": [
      "classification",
      "binary",
      "multipleInstanceLearning"
    ]
  },
  "inputs": {
    "data": [
      {
        "datasetID": "Musk_dataset",
        "targets": [
          {
            "targetIndex": 0,
            "resID": "learningData",
            "colIndex": 3,
            "colName": "bagLabel"
          }
        ]
      }
    ],
    "performanceMetrics": [
      {
        "metric": "accuracy"
      }
    ]
  }
}
```

## References
<a name="ref1"></a>Dietterich, T. G., Lathrop, R. H., & Lozano-Pérez, T. (1997). Solving the multiple instance problem with axis-parallel rectangles. Artificial intelligence, 89(1-2), 31-71.