# Creating Object Detection Datasets for D3M Systems

## Overview
This document contains information about creating image-based object detection datasets for D3M systems. Given an image, an object detector model produces bounding boxes around objects of inetrest such as pedestrian, car, bike, etc. Typically, training data for object detection task consists of (1) individual images files,  (2) one or more bounding boxes for each image, and (3) object labels associated with those bounding boxes. Follow these steps to create a D3M dataset from these elements for the task of training an object detection model.

Assume that the name of your project is `objDetect`.

### Step 1: Creating the directory structure

Create the following directory structure:
```
objDetect
  └── objDetect_dataset
    └── media
    └── tables
```

### Step 2: Placing the image files
Place all your image files in the `media` directory as shown below. 

```
objDetect
└── objDetect_dataset
    └── media
      ├── image_00.jpg
      ├── image_01.jpg
      ├── image_02.jpg
      ├── image_03.jpg
      ├── image_04.jpg
      ├── image_05.jpg
      ├── ...
      ├── ...
      └── ...
```

## Step 3: Creating the entry-point CSV File

The entry point to the dataset is through a table which associates images in the training dataset with their bounding boxes and object labels. Create a CSV file called `learningData.csv` containing the table as shown below. 

| d3mIndex |  image_file  |   object   |            bounding_box           |
|:--------:|:------------:|:----------:|:---------------------------------:|
| 0        | image_00.jpg | pedestrian | "160,182,160,431,302,431,302,182" |
| 0        | image_00.jpg | car        | "420,171,420,486,535,486,535,171" |
| 1        | image_01.jpg | car        | "68,93,68,380,191,380,191,93"     |
| 2        | image_02.jpg | car        | "293,135,293,421,447,421,447,135" |
| 3        | image_03.jpg | bike       | "168,60,168,338,324,338,324,60"   |
| 3        | image_03.jpg | car        | "9,61,9,180,48,180,48,61"         |
| 4        | image_04.jpg | bike       | "188,59,188,336,320,336,320,59"   |
| 4        | image_04.jpg | pedestrian | "2,53,2,158,40,158,40,53"         |
| 5        | image_05.jpg | pedestrian | "208,108,208,385,346,385,346,108" |
| 5        | image_05.jpg | pedestrian | "2,108,2,384,87,384,87,108"       |
| ...      | ...          | ...        | ...                               |
| ...      | ...          | ...        | ...                               |

The columns in this table are:

- `d3mIndex`: This is a required multi-index column which relates rows to images in the `media` directory and objects of interest and corresponding bounding boxes. Because it is a multi-index column, it can contain non-unique values, but cannot contain NULL values.
- `image_file`: Each entry in this column points to an image and contains the name of image file. 
- `object`: This is a label signifying an object of interest. In case of a single-class object detection problem, this column can be omitted.
-  `bounding_box`: Rectangular regions in the image specified by 8 coordinate values corresponding to 4 vertices.  The vertices are ordered counter-clockwise.

Save this table as a CSV file under the `tables` directory as shown below. 

```
objDetect
└── objDetect_dataset
    └── media
    │   ├── image_00.jpg
    │   ├── image_01.jpg
    │   ├── image_02.jpg
    │   ├── image_03.jpg
    │   ├── image_04.jpg
    │   ├── image_05.jpg
    │   ├── ...
    │   ├── ...
    │   └── ...
    └── tables
      └── learningData.csv
```

### Step 4: Create a dataset schema
In addition to the raw image files and the CSV file, D3M datasets require a dataset schema (JSON) file. Create a file called `datasetDoc.json` and save it under `xView_dataset` directory as shown below.

```
objDetect
└── objDetect_dataset
    └── media
    │   ├── image_00.jpg
    │   ├── image_01.jpg
    │   ├── image_02.jpg
    │   ├── image_03.jpg
    │   ├── image_04.jpg
    │   ├── image_05.jpg
    │   ├── ...
    │   ├── ...
    │   └── ...
    └── tables
    │   └── learningData.csv
    └── datasetDoc.json
```

The dataset schema definition can be found in the official [D3M schema definition](https://gitlab.com/datadrivendiscovery/data-supply/-/blob/shared/documentation/datasetSchema.md) document. Refer to this manual for full details regarding required fields and their semantics and expected value ranges. 

But the easiest way to get started is to use the folowing JSON structure and customize it to reflect your dataset and save it into `datasetDoc.json` file. Notice that this schema captures some basic information about the dataset such as it's ID, name, and versions. It also captures additional information about the different image resources in this dataset and their formats. In the case of `learningData.csv`, it also captures information about the data type and roles of each column.

```
{
  "about": {
    "datasetID": "objDetect_dataset",
    "datasetName": "A sample object detection dataset",
    "datasetSchemaVersion": "4.1.0",
    "datasetVersion": "1.0.0"
  },
  "dataResources": [
    {
      "resID": "0",
      "resPath": "media/",
      "resType": "image",
      "resFormat": {
        "image/jpeg": [
          "jpeg"
        ]
      },
      "isCollection": true
    },
    {
      "resID": "learningData",
      "resPath": "tables/learningData.csv",
      "resType": "table",
      "resFormat": {
        "text/csv": [
          "csv"
        ]
      },
      "isCollection": false,
      "columns": [
        {
          "colIndex": 0,
          "colName": "d3mIndex",
          "colType": "integer",
          "role": [
            "multiIndex"
          ]
        },
        {
          "colIndex": 1,
          "colName": "image_file",
          "colType": "string",
          "role": [
            "attribute"
          ],
          "refersTo": {
            "resID": "0",
            "resObject": "item"
          }
        },
        {
          "colIndex": 2,
          "colName": "object",
          "colType": "categorical",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 3,
          "colName": "bounding_box",
          "colType": "realVector",
          "role": [
            "suggestedTarget",
            "boundingPolygon",
            "boundaryIndicator"
          ],
          "refersTo": {
            "resID": "learningData",
            "resObject": {
              "columnName": "image"
            }
          }
        }
      ]
    }
  ]
}
```


## Step 5: Create a problem schema

You can specify a problem associated with this dataset by creating a new directory called `objDetect_problem` under the project root as shown below. Then create a problem schema and save it in a JSON file called 'problemDoc.json' under the `objDetect_problem` directory as shown below.

```
objDetect
└── objDetect_dataset
│   └── media
│   │   ├── image_00.jpg
│   │   ├── image_01.jpg
│   │   ├── image_02.jpg
│   │   ├── image_03.jpg
│   │   ├── image_04.jpg
│   │   ├── image_05.jpg
│   │   ├── ...
│   │   ├── ...
│   │   └── ...
│   └── tables
│   │   └── learningData.csv
│   └── datasetDoc.json
└── objDetect_problem
    └── problemDoc.json
```

The full problem schema definition can be found in the official [D3M problem schema definition](https://gitlab.com/datadrivendiscovery/data-supply/-/blob/shared/documentation/problemSchema.md) document. Refer to this manual for all the details regarding required fields and their semantics and expected range of values.

But the easiest way to get started is to use the following JSON structure and customize it to reflect your problem and save it in the `problemDoc.json` file. Notice that this schema captures some basic information about the problem such as it's ID, name, and versions. It also captures information about the task (`objectDetection` on `image` data) and has a reference to the dataset part along with the information about the target column. Finally, it also captures information about the performance metric, objectDetection average precision (`objectDetectionAP`) in this case.

```
{
  "about": {
    "problemID": "objDetect_problem",
    "problemName": "a sample object detection problem",
    "problemSchemaVersion": "4.1.0",
    "problemVersion": "1.0.0",
    "taskKeywords": [
      "objectDetection",
      "image"
    ]
  },
  "inputs": {
    "data": [
      {
        "datasetID": "objDetect_dataset",
        "targets": [
          {
            "targetIndex": 0,
            "resID": "learningData",
            "colIndex": 3,
            "colName": "bounding_box"
          }
        ]
      }
    ],
    "performanceMetrics": [
      {
        "metric": "objectDetectionAP"
      }
    ]
  }
}
```