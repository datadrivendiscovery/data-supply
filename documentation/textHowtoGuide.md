# Preparation of Text Datasets for D3M Systems

## Overview
This document contains information about creating textual training datasets for D3M systems. Text datasets come in many types and formats. Here we assume that each data point or training example corresponds to a single plain text document. We also assume a supervised learning context, requiring labels associated with each text document. The labels can be categorical (classification task) or numerical (regression task).


## Step 1: Creating the directory structure
For the purposes of this guide, assume that the name of your project is `TextML`. As a first step, create the following directory structure:

```
TextML
└── TextML_dataset
    ├── tables
    └── text
```

## Step 2: Placing the text files
Place all your text files/documents in the `text` directory as shown below. 

```
TextML
└── TextML_dataset
    ├── tables
    └── text
        ├── article_00_00.txt
        ├── article_00_01.txt
        ├── article_00_02.txt
        ├── article_00_04.txt
        ├── article_00_05.txt
        ├── ...
        ├── ...
        └── ...
```

## Step 3: Creating the entry point CSV File
The entry point to the dataset is through a table which associates text files in the training dataset with their labels. Create a CSV file called `learningData.csv` containing the table as shown below. 


| d3mIndex |     text_file     | label |
|:--------:|:-----------------:|:-----:|
|     0    | article_00_00.txt |   0   |
|     1    | article_00_01.txt |   3   |
|     2    | article_00_02.txt |   0   |
|     3    | article_00_03.txt |   2   |
|    ...   |        ...        |  ...  |
|    ...   |        ...        |  ...  |
|    ...   |        ...        |  ...  |
|   3921   | article_28_77.txt |   1   |
|   3922   | article_40_49.txt |   2   |
|   3923   | article_47_61.txt |   0   |


Each row in this CSV file refers to a text file in the `text` directory. It also contains a labels column and can also optionally contain additional metadata columns (e.g., author, publication date, etc.)

The required columns of this CSV file are:

- `d3mIndex`: This is the index/primary key column which uniquely identifies each row. It must contain unique values and cannot contain NULL values.
- `text_file`: Each entry in this column points to a text and contains the name of the text file. 
- `label`: This is the label associated with the text (which is the target to be predicted by the model).

Save the `learningData.csv` in the `tables` as shown below.

```
TextML
└── TextML_dataset
    ├── tables
    │   └── learningData.csv
    └── text
        ├── article_00_00.txt
        ├── article_00_01.txt
        ├── ...
        ├── ...
        └── ...
```

## Step 4: Create the dataset schema file
In addition to the raw text files and the CSV file, D3M datasets require a dataset schema (JSON) file. Create a file called `datasetDoc.json` and save it under `TextML_dataset` directory as shown below.

```
TextML
└── TextML_dataset
    ├── datasetDoc.json
    ├── tables
    │   └── learningData.csv
    └── text
        ├── article_00_00.txt
        ├── article_00_01.txt
        ├── ...
        ├── ...
        └── ...
```

The dataset schema definition can be found in the official [D3M dataset schema definition](https://gitlab.com/datadrivendiscovery/data-supply/-/blob/shared/documentation/datasetSchema.md) document. Refer to this manual for full details regarding required fields and their semantics and expected range of values. 

But the easiest way to get started is to use the following JSON structure and customize it to reflect your dataset and save it into `datasetDoc.json` file. Notice that this schema captures some basic information about the dataset such as it's ID, name, and versions. It also captures additional information about the different resources in this dataset and their formats, and in the case of `learningData.csv` it also captures information about the data type and roles of each column.

```
{
  "about": {
    "datasetID": "TextML_dataset",
    "datasetName": "TextML dataset",
    "datasetSchemaVersion": "4.1.0",
    "datasetVersion": "1.0"
  },
  "dataResources": [
    {
      "resID": "0",
      "resPath": "text/",
      "resType": "text",
      "resFormat": {
        "text/plain": [
          "txt"
        ]
      },
      "isCollection": true
    },
    {
      "resID": "learningData",
      "resPath": "tables/learningData.csv",
      "resType": "table",
      "resFormat": {
        "text/csv": [
          "csv"
        ]
      },
      "isCollection": false,
      "columns": [
        {
          "colIndex": 0,
          "colName": "d3mIndex",
          "colType": "integer",
          "role": [
            "index"
          ]
        },
        {
          "colIndex": 1,
          "colName": "text_file",
          "colType": "string",
          "role": [
            "attribute"
          ],
          "refersTo": {
            "resID": "0",
            "resObject": "item"
          }
        },
        {
          "colIndex": 2,
          "colName": "label",
          "colType": "categorical",
          "role": [
            "suggestedTarget"
          ]
        }
      ]
    }
  ]
}
``` 

## Step 5: Create the problem schema

Aditionally, you can specify a problem associated with this data. To specify a problem, create a new directory called `TextML_problem` under the project root as shown below. Then create a problem schema and save it in a JSON file called 'problemDoc.json' under the `TextML_problem` directory as shown below.

```
TextML
└── TextML_dataset
│   ├── datasetDoc.json
│   ├── tables
│   │   └── learningData.csv
│   └── text
│       ├── article_00_00.txt
│       ├── article_00_01.txt
│       ├── ...
│       ├── ...
│       └── ...
└── TextML_problem
    └── problemDoc.json
```

The full problem schema definition can be found in the official [D3M problem schema definition](https://gitlab.com/datadrivendiscovery/data-supply/-/blob/shared/documentation/problemSchema.md) document. Refer to this manual for all the details regarding required fields and their semantics and expected range of values.

But the easiest way to get started is to use the following JSON structure and customize it to reflect your problem and save it in the `problemDoc.json` file. Notice that this schema captures some basic information about the problem such as it's ID, name, and versions. It also captures information about the task (multi-class classification on text data) and has a reference to the dataset part along with the information about the target column. Finally, it also captures information about the performance metric (f1Macro in this case).

```
{
  "about": {
    "problemID": "TextML_problem",
    "problemName": "a sample text classification problem",
    "problemVersion": "1.0.0",
    "problemSchemaVersion": "4.1.0",
    "taskKeywords": [
      "classification",
      "multiClass",
      "text"
    ]
  },
  "inputs": {
    "data": [
      {
        "datasetID": "TextML_dataset",
        "targets": [
          {
            "targetIndex": 0,
            "resID": "learningData",
            "colIndex": 2,
            "colName": "label"
          }
        ]
      }
    ],
    "performanceMetrics": [
      {
        "metric": "f1Macro"
      }
    ]
  }
}
```