# Relational database style multi-table datasets for D3M

Here we illustrate the key steps involved in preparing a relational database style multi-table dataset for D3M systems. We will illustrate these steps using the example of [World Development Indicators data](https://datacatalog.worldbank.org/dataset/world-development-indicators). But these steps are generalizable to other datasets.

Consider the following data which captures a portion of World Development Indicators data data. This has three tables: Indicators, Country, and Series.

| CountryName    | CountryCode | IndicatorName                                              | IndicatorCode  | Year | Value    |
|----------------|-------------|------------------------------------------------------------|----------------|------|----------|
| Vietnam        | VNM         | Trade (% of GDP)                                           | NE.TRD.GNFS.ZS | 2005 | 130.7148 |
| Malawi         | MWI         | PPG, multilateral (AMT, current US$)                       | DT.AMT.MLAT.CD | 2002 | 8280000  |
| New Zealand    | NZL         | Surface area (sq. km)                                      | AG.SRF.TOTL.K2 | 2005 | 267710   |
| Myanmar        | MMR         | Principal arrears, official creditors (current US$)        | DT.AXA.OFFT.CD | 2007 | 3.44E+09 |
| Samoa          | WSM         | CPIA economic management cluster average (1=low to 6=high) | IQ.CPA.ECON.XQ | 2011 | 4.5      |
| Czech Republic | CZE         | GNI (constant LCU)                                         | NY.GNP.MKTP.KN | 2009 | 3.59E+12 |
| Bangladesh     | BGD         | Terms of trade adjustment (constant LCU)                   | NY.TTF.GNFS.KN | 1998 | 1.36E+10 |
| Mozambique     | MOZ         | Gross domestic income (constant 2005 US$)                  | NY.GDY.TOTL.KD | 1998 | 3.83E+12 |
| Guyana         | GUY         | Lifetime risk of maternal death (%)                        | SH.MMR.RISK.ZS | 2001 | 0.670581 |



| CountryCode | ShortName           | CurrencyUnit          | Region                     | Seriescode        | IncomeGroup          | … | … | … | SystemOfTrade        | GovernmentAccountingConcept     | LatestAgriculturalCensus | LatestIndustrialData | LatestTradeData | LatestWaterWithdrawalData |
|-------------|---------------------|-----------------------|----------------------------|-------------------|----------------------|---|---|---|----------------------|---------------------------------|--------------------------|----------------------|-----------------|---------------------------|
| AFG         | Afghanistan         | Afghan afghani        | South Asia                 | BN.TRF.KOGT.CD    | Low income           | … | … | … | General trade system | Consolidated central government | 2013/14                  |                      | 2013            | 2000                      |
| ALB         | Albania             | Albanian lek          | Europe & Central Asia      | BN.KAC.EOMS.CD    | Upper middle income  | … | … | … | General trade system | Budgetary central government    | 2012                     | 2011                 | 2013            | 2006                      |
| DZA         | Algeria             | Algerian dinar        | Middle East & North Africa | BN.FIN.TOTL.CD    | Upper middle income  | … | … | … | Special trade system | Budgetary central government    |                          | 2010                 | 2013            | 2001                      |
| ASM         | American Samoa      | U.S. dollar           | East Asia & Pacific        | BX.PEF.TOTL.CD.WD | Upper middle income  | … | … | … | Special trade system |                                 | 2007                     |                      |                 |                           |
| ADO         | Andorra             | Euro                  | Europe & Central Asia      | BN.KLT.PTXL.CD    | High income: nonOECD | … | … | … | Special trade system |                                 |                          |                      | 2006            |                           |
| AGO         | Angola              | Angolan kwanza        | Sub-Saharan Africa         | BX.KLT.DREM.CD.DT | Upper middle income  | … | … | … | Special trade system | Budgetary central government    | 2015                     |                      |                 | 2005                      |
| ATG         | Antigua and Barbuda | East Caribbean dollar | Latin America & Caribbean  | BN.RES.INCL.CD    | High income: nonOECD | … |   | … | General trade system | Budgetary central government    | 2007                     |                      | 2013            | 2005                      |
| …           | …                   | …                     | …                          |                   | …                    | … | … | … | …                    | …                               | …                        | …                    | …               | …                         |
| …           | …                   | …                     | …                          |                   | …                    | … | … | … | …                    | …                               | …                        | …                    | …               | …                         |


| SeriesCode           | Topic                                                                      | IndicatorName                                             | ShortDefinition | LongDefinition | RelatedIndicators | LicenseType |
|----------------------|----------------------------------------------------------------------------|-----------------------------------------------------------|-----------------|----------------|-------------------|-------------|
| BN.KLT.DINV.CD       | Economic Policy & Debt: Balance of payments: Capital & financial   account | Foreign direct investment, net (BoP, current US$)         | …               | …              | …                 | Open        |
| BX.KLT.DINV.WD.GD.ZS | Economic Policy & Debt: Balance of payments: Capital & financial   account | Foreign direct investment, net inflows (% of GDP)         | …               | …              | …                 |             |
| BX.KLT.DINV.CD.WD    | Economic Policy & Debt: Balance of payments: Capital & financial   account | Foreign direct investment, net inflows (BoP, current US$) | …               | …              | …                 |             |
| BM.KLT.DINV.GD.ZS    | Economic Policy & Debt: Balance of payments: Capital & financial   account | Foreign direct investment, net outflows (% of GDP)        | …               | …              | …                 | Open        |
| BN.TRF.KOGT.CD       | Economic Policy & Debt: Balance of payments: Capital & financial   account | Net capital account (BoP, current US$)                    | …               | …              | …                 | Open        |
| BN.KAC.EOMS.CD       | Economic Policy & Debt: Balance of payments: Capital & financial   account | Net errors and omissions (BoP, current US$)               | …               | …              | …                 | Open        |
| BN.FIN.TOTL.CD       | Economic Policy & Debt: Balance of payments: Capital & financial   account | Net financial account (BoP, current US$)                  | …               | …              | …                 | Open        |
| BX.PEF.TOTL.CD.WD    | Economic Policy & Debt: Balance of payments: Capital & financial   account | Portfolio equity, net inflows (BoP, current US$)          | …               | …              | …                 |             |
| BN.KLT.PTXL.CD       | Economic Policy & Debt: Balance of payments: Capital & financial   account | Portfolio Investment, net (BoP, current US$)              | …               | …              | …                 | Open        |
| BX.KLT.DREM.CD.DT    | Economic Policy & Debt: Balance of payments: Capital & financial   account | Primary income on FDI, payments (current US$)             | …               | …              | …                 | Open        |
| BN.RES.INCL.CD       | Economic Policy & Debt: Balance of payments: Capital & financial   account | Reserves and related items (BoP, current US$)             | …               | …              | …                 | Open        |
| BN.CAB.XOKA.GD.ZS    | Economic Policy & Debt: Balance of payments: Current account:   Balances   | Current account balance (% of GDP)                        | …               | …              | …                 | Open        |

The first table is `Country` table and captures information about different countries. The second table is the `Series` table and captures information about different economic series. There is a many-to-one relationship between `Country` and `Series` and are connected by the `Country`.`Seriescode` foreign key. Finally, there is the `Indicators` table that captures different indicators and values for each country. There is a many-to-one relationship between this table and `Country` table and are connected by `Indicators`.`Countrycode` foreign key. 

#### Indicators oo---o> Country oo---o> Series

Here are the key steps to prepare this data 

## Key steps:
1. [Create directory structure](#step1)
2. [Save the data tables as CSV files](#step2)
3. [Create a dataset schema](#step3)
4. [Create a problem schema](#step4)


<a name="step1"></a>
## Step 1: Create directory structure
As a first step, create the following directory structure.

```
world_development_indicators
└── world_development_indicators_dataset
    └── tables
```

<a name="step2"></a>
## Step 2: Save the data tables as a CSV files.

Save the `Country` and `Series` data tables as a CSV files to the `tables` directory as shown.

```
world_development_indicators
└── world_development_indicators_dataset
    └── tables
        └── country.csv
        └── series.csv
```

Because `Indicators` is the entry point to the learning data, add a special `d3mIndex` column to the original `Indicators` table and populate it with row IDs starting with 0 as shown below.

| d3mIndex | CountryName    | CountryCode | IndicatorName                                              | IndicatorCode  | Year | Value    |
|----------|----------------|-------------|------------------------------------------------------------|----------------|------|----------|
| 4109510  | Vietnam        | VNM         | Trade (% of GDP)                                           | NE.TRD.GNFS.ZS | 2005 | 130.7148 |
| 3560863  | Malawi         | MWI         | PPG, multilateral (AMT, current US$)                       | DT.AMT.MLAT.CD | 2002 | 8280000  |
| 4058250  | New Zealand    | NZL         | Surface area (sq. km)                                      | AG.SRF.TOTL.K2 | 2005 | 267710   |
| 4412324  | Myanmar        | MMR         | Principal arrears, official creditors (current US$)        | DT.AXA.OFFT.CD | 2007 | 3.44E+09 |
| 5152999  | Samoa          | WSM         | CPIA economic management cluster average (1=low to 6=high) | IQ.CPA.ECON.XQ | 2011 | 4.5      |
| 4709620  | Czech Republic | CZE         | GNI (constant LCU)                                         | NY.GNP.MKTP.KN | 2009 | 3.59E+12 |
| 2908007  | Bangladesh     | BGD         | Terms of trade adjustment (constant LCU)                   | NY.TTF.GNFS.KN | 1998 | 1.36E+10 |
| 2974821  | Mozambique     | MOZ         | Gross domestic income (constant 2005 US$)                  | NY.GDY.TOTL.KD | 1998 | 3.83E+12 |
| 3384818  | Guyana         | GUY         | Lifetime risk of maternal death (%)                        | SH.MMR.RISK.ZS | 2001 | 0.670581 |
| …        | …              | …           | …                                                          | …              | …    | …        |
| …        | …              | …           | …                                                          | …              | …    | …        |
| …        | …              | …           | …                                                          | …              | …    | …        |
| …        | …              | …           | …                                                          | …              | …    | …        |

Save this table in a special CSV file called `learningData.csv` under the `tables` directory as shown below.

```
world_development_indicators
└── world_development_indicators_dataset
    └── tables
        └── country.csv
        └── series.csv
        └── learningData.csv
```

<a name="step3"></a>
## Step 3: Create a dataset schema file

In addition to the CSV file, D3M datasets require a dataset schema (JSON) file. Create a file called `datasetDoc.json` and save it under `sunspots_dataset` directory as shown below.

```
world_development_indicators
└── world_development_indicators_dataset
    └── tables
    │   └── Country.csv
    │   └── Series.csv
    │   └── learningData.csv
    └── datasetDoc.json
```

This JSON file should contain metadata about your dataset in a machine readable form and comply with the official [D3M dataset schema definition](https://gitlab.com/datadrivendiscovery/data-supply/-/blob/shared/documentation/datasetSchema.md). Refer to this manual for details regarding the required fields, their semantics, and their expected range of values.

But the easiest way to get started is to use the following JSON structure and customize it to reflect your dataset and the columns in your data. Then save it in the `datasetDoc.json` file. Notice that this schema captures some basic information about the dataset such as it's ID, name, and versions. It also captures additional information about the table such as column names, their data types, and roles. 

```
{
  "about": {
    "datasetID": "world_development_indicators_dataset",
    "datasetName": "World development indicators: Life expectancy prediction dataset",
    "datasetSchemaVersion": "4.0.0",
    "datasetVersion": "1.0.0"
  },
  "dataResources": [
    {
      "resID": "0",
      "resPath": "tables/Country.csv",
      "resType": "table",
      "resFormat": {
        "text/csv": [
          "csv"
        ]
      },
      "isCollection": false,
      "columns": [
        {
          "colIndex": 0,
          "colName": "CountryCode",
          "colType": "categorical",
          "role": [
            "index"
          ]
        },
        {
          "colIndex": 1,
          "colName": "ShortName",
          "colType": "string",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 2,
          "colName": "TableName",
          "colType": "string",
          "role": [
            "attribute"
          ]
        },
        ...
        ...
        {
          "colIndex": 4,
          "colName": "Seriescode",
          "colType": "categorical",
          "role": [
            "attribute"
          ],
          "refersTo": {
            "resID": "1",
            "resObject": {
              "columnName": "SeriesCode"
            }
          }
        },
        ...
        ...
        {
          "colIndex": 29,
          "colName": "LatestTradeData",
          "colType": "string",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 30,
          "colName": "LatestWaterWithdrawalData",
          "colType": "string",
          "role": [
            "attribute"
          ]
        }
      ]
    },
    {
      "resID": "1",
      "resPath": "tables/Series.csv",
      "resType": "table",
      "resFormat": {
        "text/csv": [
          "csv"
        ]
      },
      "isCollection": false,
      "columns": [
        {
          "colIndex": 0,
          "colName": "SeriesCode",
          "colType": "categorical",
          "role": [
            "index"
          ]
        },
        {
          "colIndex": 1,
          "colName": "Topic",
          "colType": "string",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 2,
          "colName": "IndicatorName",
          "colType": "string",
          "role": [
            "attribute"
          ]
        },
        ...
        ...
        {
          "colIndex": 18,
          "colName": "RelatedIndicators",
          "colType": "string",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 19,
          "colName": "LicenseType",
          "colType": "categorical",
          "role": [
            "attribute"
          ]
        }
      ],
    },
    {
      "resID": "learningData",
      "resPath": "tables/learningData.csv",
      "resType": "table",
      "resFormat": {
        "text/csv": [
          "csv"
        ]
      },
      "isCollection": false,
      "columns": [
        {
          "colIndex": 0,
          "colName": "d3mIndex",
          "colType": "integer",
          "role": [
            "index"
          ]
        },
        {
          "colIndex": 1,
          "colName": "CountryName",
          "colType": "categorical",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 2,
          "colName": "CountryCode",
          "colType": "categorical",
          "role": [
            "attribute"
          ],
          "refersTo": {
            "resID": "0",
            "resObject": {
              "columnName": "CountryCode"
            }
          }
        },
        {
          "colIndex": 3,
          "colName": "IndicatorName",
          "colType": "string",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 4,
          "colName": "IndicatorCode",
          "colType": "categorical",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 5,
          "colName": "Year",
          "colType": "dateTime",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 6,
          "colName": "Value",
          "colType": "real",
          "role": [
            "suggestedTarget"
          ]
        }
      ]
    }
  ]
}
```

Note that the metadata for the `Seriescode` column in `country.csv` resource has `refersTo` information and points to the `series.csv` resource through the `Seriescode` foreign key. Similarly, the metadata for the `CountryCode` column in `learningData.csv` resource has `refersTo` information and points to the `country.csv` resource through the `CountryCode` foreign key.

<a name="step4"></a>
## Step 4: Create a problem schema

To specify a forecasting problem over this data, create a new directory called `world_development_indicators_problem` under the project root as shown below. Then create a problem schema and save it in a JSON file called `problemDoc.json` under the `world_development_indicators_problem` directory as shown below.

```
world_development_indicators
└── world_development_indicators_dataset
│   └── tables
│   │   └── Country.csv
│   │   └── Series.csv
│   │   └── learningData.csv
│   └── datasetDoc.json
└── world_development_indicators_problem
    └── problemDoc.json
```

The full problem schema definition can be found in the official [D3M problem schema definition](https://gitlab.com/datadrivendiscovery/data-supply/-/blob/shared/documentation/problemSchema.md) document. Refer to this manual for details regarding the contents of this `problemDoc.json` file.

But the easiest way to get started is to use the following JSON structure and customize it to reflect your problem and save it in the `problemDoc.json` file. Notice that this schema captures some basic information about the problem such as it's ID, name, and versions. It also captures information about the task (`univariate` `regression` qualified by `relational` tables context) and has a reference to the dataset part along with some information about the target column. Finally, it also captures information about the performance metric (root mean squared error, in this case).

```
{
  "about": {
    "problemID": "world_development_indicators_problem",
    "problemName": "Life expectancy prediction problem",
    "problemSchemaVersion": "4.0.0",
    "problemVersion": "1.0.0",
    "taskKeywords": [
      "regression",
      "univariate",
      "relational"
    ]
  },
  "inputs": {
    "data": [
      {
        "datasetID": "world_development_indicators_dataset",
        "targets": [
          {
            "targetIndex": 0,
            "resID": "learningData",
            "colIndex": 6,
            "colName": "Value"
          }
        ]
      }
    ],
    "performanceMetrics": [
      {
        "metric": "rootMeanSquaredError"
      }
    ]
  }
}
```