# Preparation of Tabular Datasets for D3M

## Overview
This document contains information about creating tabular training datasets for D3M systems. We assume that the tabular data is in CSV format. We also assume a supervised learning context, requiring a column containing labels for each row. The labels can be categorical (classification task) or numerical (regression task).

For the purposes of illustration of the steps involved in the preparation of a dataset, let's assume that you have the following tabular data containing statistics of baseball players in MLB.

| Player         | Number_seasons | Games_played | Runs | Home_runs | Batting_average | Position    | Hall_of_Fame |
|----------------|----------------|--------------|------|-----------|-----------------|-------------|--------------|
| HANK_AARON     | 23             | 3298         | 2174 | 755       | 0.305           | Outfield    | 1            |
| JERRY_ADAIR    | 13             | 1165         | 378  | 57        | 0.254           | Second_base | 0            |
| BOBBY_ADAMS    | 14             | 1281         | 591  | 37        | 0.269           | Third_base  | 1            |
| JOE_ADCOCK     | 17             | 1959         | 823  | 336       | 0.277           | First_base  | 0            |
| TOMMIE_AGEE    | 12             | 1129         | 558  | 130       | 0.255           | Outfield    | 0            |
| LUIS_AGUAYO    | 10             | 568          | 142  | 37        | 0.236           | Shortstop   | 0            |
| EDDIE_AINSMITH | 15             | 1078         | 299  | 22        | 0.232           | Catcher     | 1            |
| BERNIE_ALLEN   | 12             | 1139         | 357  | 73        | 0.239           | Second_base | 0            |
| DICK_ALLEN     | 15             | 1749         | 1099 | 351       | 0.292           | First_base  | 1            |
| ...            | ...            | ...          | ...  | ...       | ...             | ...         | ...          |
| ...            | ...            | ...          | ...  | ...       | ...             | ...         | ...          |

Further, let's assume that the `Hall_of_Fame` column is the column of known labels at training time and the target of prediction at testing time.

## Step 1: Creating the directory structure
As a first step, create the following directory structure:

```
baseball
└── baseball_dataset
    └── tables
```

## Step 2: Including the CSV file
Add a special `d3mIndex` column to the original table and populate it with row IDs starting with 0. 

| d3mIndex | Player         | Number_seasons | Games_played | Runs | Home_runs | Batting_average | Position    | Hall_of_Fame |
|----------|----------------|----------------|--------------|------|-----------|-----------------|-------------|--------------|
| 0        | HANK_AARON     | 23             | 3298         | 2174 | 755       | 0.305           | Outfield    | 1            |
| 1        | JERRY_ADAIR    | 13             | 1165         | 378  | 57        | 0.254           | Second_base | 0            |
| 3        | BOBBY_ADAMS    | 14             | 1281         | 591  | 37        | 0.269           | Third_base  | 1            |
| 4        | JOE_ADCOCK     | 17             | 1959         | 823  | 336       | 0.277           | First_base  | 0            |
| 5        | TOMMIE_AGEE    | 12             | 1129         | 558  | 130       | 0.255           | Outfield    | 0            |
| 6        | LUIS_AGUAYO    | 10             | 568          | 142  | 37        | 0.236           | Shortstop   | 0            |
| 7        | EDDIE_AINSMITH | 15             | 1078         | 299  | 22        | 0.232           | Catcher     | 1           |
| 8        | BERNIE_ALLEN   | 12             | 1139         | 357  | 73        | 0.239           | Second_base | 0            |
| 10       | DICK_ALLEN     | 15             | 1749         | 1099 | 351       | 0.292           | First_base  | 1            |
| ...      | ...            | ...            | ...          | ...  | ...       | ...             | ...         | ...          |
| ...      | ...            | ...            | ...          | ...  | ...       | ...             | ...         | ...          |

Save the table in a CSV file called `learningData.csv` under the `tables` directory as shown below.

```
baseball
└── baseball_dataset
    └── tables
        └── learningData.csv
```

## Step 3: Create the dataset schema file

In addition to the CSV file, D3M datasets require a dataset schema (JSON) file. Create a file called `datasetDoc.json` and save it under `baseball_dataset` directory as shown below.

```
baseball
└── baseball_dataset
    └── tables
    │   └── learningData.csv
    └── datasetDoc.json
```

The dataset schema definition can be found in the official [D3M dataset schema definition](https://gitlab.com/datadrivendiscovery/data-supply/-/blob/shared/documentation/datasetSchema.md) document. Refer to this manual for all the details regarding required fields and their semantics and range of values.

But the easiest way to get started is to use the following JSON structure and customize it to reflect your dataset and save it in the `datasetDoc.json` file. Notice that this schema captures some basic information about the dataset such as it's ID, name, and versions. It also captures additional information about the resources in this dataset (in this case there is only one resource, `learningData.csv`) and information about the columns in this resource such as a column's data type and role.

```
{
  "about": {
    "datasetID": "185_baseball_dataset",
    "datasetName": "baseball hall of fame statistics",
    "datasetSchemaVersion": "4.1.0",
    "datasetVersion": "1.0.0"
  },
  "dataResources": [
    {
      "resID": "learningData",
      "resPath": "tables/learningData.csv",
      "resType": "table",
      "resFormat": {
        "text/csv": [
          "csv"
        ]
      },
      "isCollection": false,
      "columns": [
        {
          "colIndex": 0,
          "colName": "d3mIndex",
          "colType": "integer",
          "role": [
            "index"
          ]
        },
        {
          "colIndex": 1,
          "colName": "Player",
          "colType": "categorical",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 2,
          "colName": "Number_seasons",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 3,
          "colName": "Games_played",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 4,
          "colName": "Runs",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 5,
          "colName": "Home_runs",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 6,
          "colName": "Batting_average",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 7,
          "colName": "Position",
          "colType": "categorical",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 8,
          "colName": "Hall_of_Fame",
          "colType": "categorical",
          "role": [
            "suggestedTarget"
          ]
        }
      ]
    }
  ]
}
```

For more information about the semantics of all the fields in this schema, please refer to the official dataset schema definition [D3M dataset schema definition file here](https://gitlab.com/datadrivendiscovery/data-supply/-/blob/shared/documentation/datasetSchema.md).


## Step 4: Create the problem schema

Aditionally, you can specify a problem associated with this data. In this illustration, we assume that the problem (or the machine learning task) is to predict if a player will make it into the hall of fame records. Therefore, the `Hall_of_Fame` column becomes the target of prediction  (0: NO, 1: YES).

To specify a problem, create a new directory called `baseball_problem` under the project root as shown below. Then create a problem schema and save it in a JSON file called 'problemDoc.json' under the `baseball_problem` directory as shown below.

```
baseball
└── baseball_dataset
    └── tables
    │   └── learningData.csv
    └── datasetDoc.json
└── baseball_problem
    └── problemDoc.json
```

The full problem schema definition can be found in the official [D3M problem schema definition](https://gitlab.com/datadrivendiscovery/data-supply/-/blob/shared/documentation/problemSchema.md) document. Refer to this manual for all the details regarding required fields and their semantics and range of values.

But the easiest way to get started is to use the following JSON structure and customize it to reflect your problem and save it in the `problemDoc.json` file. Notice that this schema captures some basic information about the problem such as it's ID, name, and versions. It also captures information about the task (binary classification on tabular data) and has a reference to the dataset part along with the information about the target column. Finally, it also captures information about the performance metric (f1 in this case).

```
{
  "about": {
    "problemID": "185_baseball_problem",
    "problemName": "baseball hall of famer prediction",
    "problemVersion": "1.0.0",
    "problemSchemaVersion": "4.1.0",
    "taskKeywords": [
      "classification",
      "binary",
      "tabular"
    ]
  },
  "inputs": {
    "data": [
      {
        "datasetID": "185_baseball_dataset",
        "targets": [
          {
            "targetIndex": 0,
            "resID": "learningData",
            "colIndex": 8,
            "colName": "Hall_of_Fame"
          }
        ]
      }
    ],
    "performanceMetrics": [
      {
        "metric": "f1"
      }
    ]
  }
}
```

