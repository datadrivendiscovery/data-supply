# Preparation of Time Series Forecasting Datasets for D3M

Here we illustrate the key steps involved in preparing a time series dataset for a forecasting problem for D3M systems. We will illustrate these steps using the example of sunspots data. But these steps are generalizable to other datasets.

Consider the following data which captures the historical occurence of sunspots on a monthly basis.

| year-month | sunspots |
|------------|----------|
| 1749-01    | 96.7     |
| 1749-02    | 104.3    |
| 1749-03    | 116.7    |
| 1749-04    | 92.8     |
| 1749-05    | 141.7    |
| 1749-06    | 139.2    |
| 1749-07    | 158.0    |
| 1749-08    | 110.5    |
| 1749-09    | 126.5    |
| ...        | ...      |
| ...        | ...      |

Here are the key steps to prepare this data 

## Key steps:
1. [Create directory structure](#step1)
2. [Save the data table as a CSV file](#step2)
3. [Create a dataset schema](#step3)
4. [Create a problem schema](#step4)


<a name="step1"></a>
## Step 1: Create directory structure
As a first step, create the following directory structure.

```
sunspots
└── sunspots_dataset
    └── tables
```
<a name="step2"></a>
## Step 2: Save the data table as a CSV file

Add a special `d3mIndex` column to the original table and populate it with row IDs starting with 0 as shown below.

| d3mIndex | year-month | sunspots |
|----------|------------|----------|
| 0        | 1749-01    | 96.7     |
| 1        | 1749-02    | 104.3    |
| 2        | 1749-03    | 116.7    |
| 3        | 1749-04    | 92.8     |
| 4        | 1749-05    | 141.7    |
| 5        | 1749-06    | 139.2    |
| 6        | 1749-07    | 158.0    |
| 7        | 1749-08    | 110.5    |
| 8        | 1749-09    | 126.5    |
| ...      | ...        | ...      |
| ...      | ...        | ...      |

Save the table in a CSV file called `learningData.csv` under the `tables` directory as shown below.

```
sunspots
└── sunspots_dataset
    └── tables
        └── learningData.csv
```

<a name="step3"></a>
## Step 3: Create a dataset schema file

In addition to the CSV file, D3M datasets require a dataset schema (JSON) file. Create a file called `datasetDoc.json` and save it under `sunspots_dataset` directory as shown below.

```
sunspots
└── sunspots_dataset
    └── tables
    │   └── learningData.csv
    └── datasetDoc.json
```

This JSON file should contain metadata about your dataset in a machine readable form and comply with the official [D3M dataset schema definition](https://gitlab.com/datadrivendiscovery/data-supply/-/blob/shared/documentation/datasetSchema.md). Refer to this manual for details regarding the required fields, their semantics, and their expected range of values.

But the easiest way to get started is to use the following JSON structure and customize it to reflect your dataset and the columns in your data. Then save it in the `datasetDoc.json` file. Notice that this schema captures some basic information about the dataset such as it's ID, name, and versions. It also captures additional information about the table such as column names, their data types, and roles. 
```
{
  "about": {
    "datasetID": "sunspots_dataset",
    "datasetName": "monthly sunspots data",
    "datasetSchemaVersion": "4.1.0",
    "datasetVersion": "1.0.0"
  },
  "dataResources": [
    {
      "resID": "learningData",
      "resPath": "tables/learningData.csv",
      "resType": "table",
      "resFormat": {
        "text/csv": [
          "csv"
        ]
      },
      "isCollection": false,
      "columns": [
        {
          "colIndex": 0,
          "colName": "d3mIndex",
          "colType": "integer",
          "role": [
            "index"
          ]
        },
        {
          "colIndex": 1,
          "colName": "year-month",
          "colType": "dateTime",
          "role": [
            "attribute",
            "timeIndicator"
          ]
        },
        {
          "colIndex": 2,
          "colName": "sunspots",
          "colType": "real",
          "role": [
            "suggestedTarget"
          ]
        }
      ]
    }
  ]
}
```

Note that `year-month` column has a `timeIndicator` role in addition to being an attribute. If there are additional columns in your table, this json structure will have additional entries for those columns, including information for `colIndex`, `colName`, `colType`, and `role` for each column as shown in this [example](tabularHowtoGuide.md).

<a name="step4"></a>
## Step 4: Create a problem schema

To specify a forecasting problem over this data, create a new directory called `sunspots_problem` under the project root as shown below. Then create a problem schema and save it in a JSON file called `problemDoc.json` under the `sunspots_problem` directory as shown below.

```
sunspots
└── sunspots_dataset
│   └── tables
│   │   └── learningData.csv
│   └── datasetDoc.json
└── sunspots_problem
    └── problemDoc.json
```

The full problem schema definition can be found in the official [D3M problem schema definition](https://gitlab.com/datadrivendiscovery/data-supply/-/blob/shared/documentation/problemSchema.md) document. Refer to this manual for details regarding the contents of this `problemDoc.json` file.

But the easiest way to get started is to use the following JSON structure and customize it to reflect your problem and save it in the `problemDoc.json` file. Notice that this schema captures some basic information about the problem such as it's ID, name, and versions. It also captures information about the task (time series forecasting) and has a reference to the dataset part along with some information about the target column. Finally, it also captures information about the performance metric (root mean squared error, in this case).

```
{
  "about": {
    "problemID": "sunspots_problem",
    "problemName": "Monthly sunspots prediction problem",
    "problemSchemaVersion": "4.0.0",
    "problemVersion": "4.0.0",
    "taskKeywords": [
      "timeSeries",
      "forecasting",
      "tabular"
    ]
  },
  "inputs": {
    "data": [
      {
        "datasetID": "sunspots_dataset",
        "targets": [
          {
            "targetIndex": 0,
            "resID": "learningData",
            "colIndex": 2,
            "colName": "sunspots"
          }
        ]
      }
    ],
    "performanceMetrics": [
      {
        "metric": "rootMeanSquaredError"
      }
    ]
  }
}
```

## Grouped time series datasets

A grouped time series is a collection of time series which are grouped by a facet. The preparation of this kind of dataset is similar to the simpler case above with a few changes to the dataset schema.

For instance, consider the following stock price data for different compaines.

| d3mIndex | Company | Date      | Close  |
|----------|---------|-----------|--------|
| 0        | abbv    | 1/4/2013  | 28.81  |
| 1        | abbv    | 1/7/2013  | 28.869 |
| 2        | abbv    | 1/8/2013  | 28.242 |
| 3        | abbv    | 1/9/2013  | 28.399 |
| ...      | ...     | ...       | ...    |
| 1059     | baf     | 1/25/2005 | 10.252 |
| 1060     | baf     | 1/28/2005 | 10.244 |
| 1061     | baf     | 2/1/2005  | 10.222 |
| 1062     | baf     | 2/2/2005  | 10.222 |
| ...      | ...     | ...       | ...    |
| 3778     | caj     | 2/25/2005 | 31.988 |
| 3779     | caj     | 2/28/2005 | 32.016 |
| 3780     | caj     | 3/1/2005  | 32.48  |
| 3781     | caj     | 3/2/2005  | 32.171 |
| ...      | ...     | ...       | ...    |
| 6497     | dac     | 1/3/2007  | 23.8   |
| 6498     | dac     | 1/4/2007  | 23.9   |
| 6499     | dac     | 1/5/2007  | 23.63  |
| ...      | ...     | ...       | ...    |

Let's assume this is a time series prediciton problem where the task is to predict the daily closing value of a company's stock for Nov and Dec of a given year using closing values from Jan to Oct of that year. This clearly is a collection of time series grouped by company. 

The overall steps of (1) creating the directory structure, (2) saving the data table as a CSV file, (3) creating the dataset schema file, and (4) creating the problem schema file, remain the same resulting in the following project structure.

```
stockforecasting
└── stockforecasting_dataset
│   └── tables
│   │   └── learningData.csv
│   └── datasetDoc.json
└── stockforecasting_problem
    └── problemDoc.json
```

The schema for this dataset in `datasetsDoc.json` is shown below.

```
{
  "about": {
    "datasetID": "stockforecasting_dataset",
    "datasetName": "stock market - daily closing data",
    "datasetSchemaVersion": "4.1.0",
    "datasetVersion": "1.0.0"
  },
  "dataResources": [
    {
      "resID": "learningData",
      "resPath": "tables/learningData.csv",
      "resType": "table",
      "resFormat": {
        "text/csv": [
          "csv"
        ]
      },
      "columns": [
        {
          "colIndex": 0,
          "colName": "d3mIndex",
          "colType": "integer",
          "role": [
            "index"
          ]
        },
        {
          "colIndex": 1,
          "colName": "Company",
          "colType": "categorical",
          "role": [
            "attribute",
            "suggestedGroupingKey"
          ]
        },
        {
          "colIndex": 2,
          "colName": "Date",
          "colType": "dateTime",
          "timeGranularity": {
            "value": 1,
            "unit": "days"
          },
          "role": [
            "attribute",
            "timeIndicator"
          ]
        },
        {
          "colIndex": 3,
          "colName": "Close",
          "colType": "real",
          "role": [
            "suggestedTarget"
          ]
        }
      ]
    }
  ]
}
```

Comparing this json with the earlier json for the sunspots dataset reveals some differences. Notice the additional `Company` column to the json structure with `categorical` column type, but more importantly, `suggestedGroupingKey` column role. This indicates that grouping the table by company yields univariate time series data.

Similarly, there is a minor change in the problem schema in `problemDoc.json` for this problem. Notice the addition of `grouped` in the `taskKeywords` field.

```
{
  "about": {
    "problemID": "stockforecasting_problem",
    "problemName": "predicting stock market closing value problem",
    "problemSchemaVersion": "4.1.0",
    "problemVersion": "1.0.0",
    "taskKeywords": [
      "timeSeries",
      "forecasting",
      "tabular",
      "grouped"
    ]
  },
  "inputs": {
    "data": [
      {
        "datasetID": "stockforecasting_dataset",
        "targets": [
          {
            "targetIndex": 0,
            "resID": "learningData",
            "colIndex": 3,
            "colName": "Close"
          }
        ]
      }
    ],
    "performanceMetrics": [
      {
        "metric": "rootMeanSquaredError"
      }
    ]
  }
}
```

