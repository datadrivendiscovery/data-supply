# Preparation of Image, Video or Audio Datasets for D3M

## Overview
This document contains information about creating image/video/audio training datasets for D3M systems. Here we assume that each data point or training example corresponds to a single image/video/audio file. We also assume a supervised learning context, where labels are available for each image/video/audio file during the training process. These labels can be categorical (classification task) or numerical (regression task).

The steps for preparing a dataset will be illustrated here assuming we have image files, but are equally applicable to datasets containing video and audio files. The differences which arise in the specification of resource types and data formats will be dealt with towards the end of this guide.

## Key steps:
1. [Create directory structure](#step1)
2. [Include raw image files](#step2)
3. [Create the entry-point CSV file](#step3)
4. [Create a dataset schema](#step4)
5. [Create a problem schema](#step5)


<a name="step1"></a>
## Step 1: Create directory structure
For the purposes of this guide, assume that the name of your project is `visionML`. As a first step, create the following directory structure.

```
visionML
└── visionML_dataset
    └── media
    └── tables
```

<a name="step2"></a>
## Step 2: Include image files
Place all your raw image (or video or audio) files in the `media` directory as shown below. 

```
visionML
└── visionML_dataset
    └── media
      ├── image_00.jpg
      ├── image_01.gif
      ├── image_02.jpg
      ├── image_03.png
      ├── ...
      ├── ...
      └── ...
```

<a name="step3"></a>
## Step 3: Create the entry-point CSV file
The entry point to the dataset is through a table which associates images in the training dataset with their labels. Create a CSV file called `learningData.csv` containing the table as shown below. 

| d3mIndex |  image_file  |     label   |
|:--------:|:------------:|:-----------:|
|     0    | image_00.jpg |      0      |
|     1    | image_01.gif |      1      |
|     2    | image_02.jpg |      0      |
|     3    | image_03.png |      2      |
|    ...   |      ...     |     ...     |
|    ...   |      ...     |     ...     |
|    ...   |      ...     |     ...     |


Each row in this table refers to an image file in the `media` directory. It also contains a `label` column and can also optionally contain additional attribute columns (e.g., source, sensor parameters, location, datetime, bounding box, etc.)

But the only required columns of this table are:

- `d3mIndex`: This is the index/primary key column which uniquely identifies each row. It must contain unique values and cannot contain NULL values.
- `image_file`: Each entry in this column points to an image/video and contains the name of image/video file. 
- `label`: This is the label associated with the image/video (which is the target to be predicted by the model). 

Save the `learningData.csv` in the `tables` directory as shown below.

```
visionML
└── visionML_dataset
    └── media
    │   ├── image_00.jpg
    │   ├── ...
    │   ├── ...
    │   └── ... 
    └── tables
        └── learningData.csv
```

<a name="step4"></a>
## Step 4: Create a dataset schema
In addition to the raw image files and the CSV file, D3M datasets require a dataset schema (JSON) file. Create a file called `datasetDoc.json` and save it under `visionML_dataset` directory as shown below.

```
visionML
└── visionML_dataset
    └── media
    │   ├── image_00.jpg
    │   ├── ...
    │   ├── ...
    │   └── ...
    └── tables
    │   └── learningData.csv
    └── datasetDoc.json
```

The dataset schema definition can be found in the official [D3M dataset schema definition](https://gitlab.com/datadrivendiscovery/data-supply/-/blob/shared/documentation/datasetSchema.md) document. Refer to this manual for full details regarding required fields and their semantics and expected range of values. 

But the easiest way to get started is to use the following JSON structure and customize it to reflect your dataset and save it into `datasetDoc.json` file. Notice that this schema captures some basic information about the dataset such as it's ID, name, and versions. It also captures additional information about the different resources in this dataset and their formats, and in the case of `learningData.csv` it also captures information about the data type and roles of each column. 

```
{
  "about": {
    "datasetID": "visionML_dataset",
    "datasetName": "The visionML Dataset",
    "datasetSchemaVersion": "4.1.0",
    "datasetVersion": "1.0"
  },
  "dataResources": [
    {
      "resID": "0",
      "resPath": "media/",
      "resType": "image",
      "resFormat": {
        "image/jpeg": [
          "jpeg",
          "jpg"
        ],
        "image/png": [
          "png"
        ],
      },
      "isCollection": true
    },
    {
      "resID": "learningData",
      "resPath": "tables/learningData.csv",
      "resType": "table",
      "resFormat": {
        "text/csv": [
          "csv"
        ]
      },
      "isCollection": false,
      "columns": [
        {
          "colIndex": 0,
          "colName": "d3mIndex",
          "colType": "integer",
          "role": [
            "index"
          ]
        },
        {
          "colIndex": 1,
          "colName": "image_file",
          "colType": "string",
          "role": [
            "attribute"
          ],
          "refersTo": {
            "resID": "0",
            "resObject": "item"
          }
        },
        {
          "colIndex": 2,
          "colName": "label",
          "colType": "categorical",
          "role": [
            "suggestedTarget"
          ]
        }
      ]
    }
  ]
}
``` 

If there are multiple image formats in the collection of images, each individual format and their extensions has to be enumerated as follows.

```{
    "resID": "0",
    "resPath": "media/",
    "resType": "image",
    "resFormat": {
      "image/jpeg": [
        "jpeg",
        "jpg"
      ],
      "image/png": [
        "png"
      ],
      "image/bmp": [
        "bmp"
      ]
    }
```

### Video datasets

If the dataset contains video files rather than images (e.g., video classification task), the `media` directory would hold all your video files instead of the image files, and the above `resource` section in the `datasetDoc.json` would contain information related to video formats as shown in the example below.

```{
    "resID": "0",
    "resPath": "media/",
    "resType": "video",
    "resFormat": {
      "video/mpeg": [
        "mpeg"
      ],
      "video/mp4": [
        "mp4"
      ]
    }
```

### Audio datasets

Similarly, if the dataset contains audio files rather than images, the `media` directory would hold all your audio files , and the above `resource` section in the `datasetDoc.json` would contain information related to audio formats as shown in the example below.

```
  {
    "resID": "0",
    "resPath": "media/",
    "resType": "audio",
    "resFormat": {
      "audio/wav": [
        "wav"
      ],
      "audio/aiff": [
        "aif",
        "aiff"
      ],
      "audio/flac": [
        "flac"
      ],
      "audio/ogg": [
        "ogg"
      ],
      "audio/mpeg": [
        "mp3"
      ]
    }
```


<a name="step5"></a>
## Step 5: Create a problem schema

Aditionally, you can specify a problem associated with this dataset. To specify a problem, create a new directory called `visionML_problem` under the project root as shown below. Then create a problem schema and save it in a JSON file called 'problemDoc.json' under the `visionML_problem` directory as shown below.

```
visionML
└── visionML_dataset
│   └── media
│   │   ├── image_00.jpg
│   │   ├── ...
│   │   ├── ...
│   │   └── ...
│   └── tables
│   │   └── learningData.csv
│   └── datasetDoc.json
└── visionML_problem
    └── problemDoc.json
```

The full problem schema definition can be found in the official [D3M problem schema definition](https://gitlab.com/datadrivendiscovery/data-supply/-/blob/shared/documentation/problemSchema.md) document. Refer to this manual for all the details regarding required fields and their semantics and expected range of values.

But the easiest way to get started is to use the following JSON structure and customize it to reflect your problem and save it in the `problemDoc.json` file. Notice that this schema captures some basic information about the problem such as it's ID, name, and versions. It also captures information about the task (multi-class classification on image data) and has a reference to the dataset part along with the information about the target column. Finally, it also captures information about the performance metric (f1Macro in this case).

```
{
  "about": {
    "problemID": "visionML_problem",
    "problemName": "a sample image classification problem",
    "problemVersion": "1.0.0",
    "problemSchemaVersion": "4.1.0",
    "taskKeywords": [
      "classification",
      "multiClass",
      "image"
    ]
  },
  "inputs": {
    "data": [
      {
        "datasetID": "visionML_dataset",
        "targets": [
          {
            "targetIndex": 0,
            "resID": "learningData",
            "colIndex": 2,
            "colName": "label"
          }
        ]
      }
    ],
    "performanceMetrics": [
      {
        "metric": "f1Macro"
      }
    ]
  }
}
```