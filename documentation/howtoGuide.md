# WORK IN PROGRESS ...

This gives a step-by-step guide to preparing different types of datasets for training D3M systems.

## How to prepare datasets for

### [Tabular classification/regresison](tabularHowtoGuide.md)
### [Text classification/regression](textHowtoGuide.md)
### [Image, video or audio classification/regression](visionHowtoGuide.md)
### [Remote-sensing applications](remotesensingHowtoGuide.md)
### [Object detection task](objectDetectionHowToGuide.md)
### [Time series forecasting](timeseriesHowTo.md)
### [Multi-instance learning task](multiInstanceLearningHowToGuide.md)
### [Relational database style multi-table datasets](relationalTablesHowToGuide.md)
### [Graph problems]()

