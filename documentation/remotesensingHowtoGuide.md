# Creating Remote Sensing Training Datasets for D3M Systems

## Overview
This document contains information about preparing remote sensing training datasets for D3M systems. Remote sensing datasets come in many types and formats. D3M currently supports only raster datasets. We currently support two cases: single raster image per data point, and multiple raster files per data point.

## [Case 1](#case1) | [Case 2](#case2)

## Key steps:
1. [Create directory structure](#step1)
2. [Include raster image files](#step2)
3. [Create the entry-point CSV file](#step3)
4. [Create a dataset schema](#step4)
5. [Create a problem schema](#step5)

<a name="case1"></a>
## Case 1: Single raster image per data point

Here each data point or training example corresponds to a single independent raster file. Preparing the dataset for this case is similar to that of a [standard image dataset](https://gitlab.com/datadrivendiscovery/data-supply/-/blob/how-to-guide/documentation/visionHowtoGuide.md) with a few additional elements to consider. 

Assume that the name of your project is `xView`. 

<a name="step1"></a>
### Step 1: Creating directory structure

Create the following directory structure:
```
xView
  └── xView_dataset
    └── media
    └── tables
```

<a name="step2"></a>
### Step 2: Including raster image files
Place the image files in the `media` directory as shown below. 

```
xView
  └── xView_dataset
    └── media
      ├── S2A_MSIL2A_20170704T112111_28_77.tif
      ├── S2A_MSIL2A_20170704T112111_37_55.tif
      ├── S2A_MSIL2A_20170704T112111_40_49.tif
      ├── S2A_MSIL2A_20170704T112111_47_61.tif
      ├── ...
      └── ...	
```

<a name="step3"></a>
### Step 3: Creating the entry point CSV file

The entry point to the dataset is through a table which associates images in the training dataset with their labels. Create a CSV file called `learningData.csv` as shown below. 


| d3mIndex |              image_file              |     timestamp    |         image_coordinates         |                                 geo_coordinates                                 |        label        |
|:--------:|:------------------------------------:|:----------------:|:---------------------------------:|:-------------------------------------------------------------------------------:|:-------------------:|
| 1        | S2A_MSIL2A_20170704T112111_28_77.tif | 2017-07-04 11:21 | "160,182,160,431,302,431,302,182" | -8.612715,38.906210,-8.612715,38.916977,-8.598814,38.916977,-8.598814,38.906210 | Broad_leaved_forest |
| 2        | S2A_MSIL2A_20170704T112111_37_55.tif | 2017-07-04 11:21 | "420,171,420,486,535,486,535,171" | -8.486442,39.143620,-8.486442,39.154371,-8.472475,39.154371,-8.472475,39.143620 | Broad_leaved_forest |
| 3        | S2A_MSIL2A_20170704T112111_40_49.tif | 2017-07-04 11:21 | "68,93,68,380,191,380,191,93"     | -8.444274,39.208306,-8.444274,39.219051,-8.430289,39.219051,-8.430289,39.208306 | Broad_leaved_forest |
| 4        | S2A_MSIL2A_20170704T112111_47_61.tif | 2017-07-04 11:21 | "293,135,293,421,447,421,447,135" | -8.348185,39.078049,-8.348185,39.088783,-8.334211,39.088783,-8.334211,39.078049 | Broad_leaved_forest |
| 5        | S2A_MSIL2A_20170704T112111_52_45.tif | 2017-07-04 11:21 | "168,60,168,338,324,338,324,60"   | -8.277054,39.250640,-8.277054,39.261365,-8.263035,39.261365,-8.263035,39.250640 | Broad_leaved_forest |
| 6        | S2A_MSIL2A_20170704T112111_52_82.tif | 2017-07-04 11:21 | "9,61,9,180,48,180,48,61"         | -8.281122,38.850571,-8.281122,38.861298,-8.267183,38.861298,-8.267183,38.850571 | Broad_leaved_forest |
| 7        | S2A_MSIL2A_20170704T112111_53_45.tif | 2017-07-04 11:21 | "188,59,188,336,320,336,320,59"   | -8.263148,39.250553,-8.263148,39.261276,-8.249127,39.261276,-8.249127,39.250553 | Broad_leaved_forest |
| 8        | S2A_MSIL2A_20170704T112111_55_73.tif | 2017-07-04 11:21 | "2,53,2,158,40,158,40,53"         | -8.238599,38.947624,-8.238599,38.958346,-8.224635,38.958346,-8.224635,38.947624 | Broad_leaved_forest |
| ...      | ...                                  | ...              | ...                               | ...                                                                             | ...                 |
| ...      | ...                                  | ...              | ...                               | ...                                                                             | ...                 |
| ...      | ...                                  | ...              | ...                               | ...                                                                             | ...                 |


Each row in this CSV file refers to an image in the `media` directory. It also contains metadata and label columns associated with images as shown below.

The columns of this CSV file are:

- `d3mIndex` (required): This is the index/primary key column which uniquely identifies each row. It must contain unique values and cannot contain NULL values.
- `image_file` (required): Each entry in this column points to an image and contains the name of the image file. 
- `timestamp` (optional): Timestamp metadata associated with images are contained in this column.
- `img_coordinates` (optional): Bounding box specified by X,Y coordinates in the image space. If unspecified, the bounding box is the whole image. The vertices are ordered counter-clockwise. For e.g., a bounding box consisting of 4 vertices and 8 coordinates: "0,160,156,182,276,302,18,431"
- `geo_coordinates` (optional): Coordinates in spatial reference system corresponding to the bounding box points. Consists of the same number of vertices as the `image_coordinates`. For instance, for UTM longitute-latitute values, it could be "-8.612715,38.906210,-8.612715,38.916977,-8.598814,38.916977,-8.598814,38.906210". 
- `label` (required): This is the label associated with the image which is the target column to be predicted by the model.

Save the table as a CSV file under the `tables` directory as shown below. 

```
xView
  └── xView_dataset
    └── media
    │  ├── S2A_MSIL2A_20170704T112111_28_77.tif
    │  ├── S2A_MSIL2A_20170704T112111_37_55.tif
    │  ├── ...
    │  ├── ...
    │  └── ...	
    └── tables
      └── learningData.csv
```

<a name="step4"></a>

### Step 4: Create a dataset schema file
In addition to the raw image files and the CSV file, D3M datasets require a dataset schema (JSON) file. Create a file called `datasetDoc.json` and save it under `xView_dataset` directory as shown below.

```
xView
  └── xView_dataset
    └── media
    │   ├── S2A_MSIL2A_20170704T112111_28_77.tif
    │   ├── S2A_MSIL2A_20170704T112111_37_55.tif
    │   ├── ...
    │   ├── ...
    │   └── ...	
    └── tables
    │   └── learningData.csv
    └── datasetDoc.json
```

The dataset schema definition can be found in the official [D3M schema definition](https://gitlab.com/datadrivendiscovery/data-supply/-/blob/shared/documentation/datasetSchema.md) document. Refer to this manual for full details regarding required fields and their semantics and expected value ranges. 

But the easiest way to get started is to use the folowing JSON structure and customize it to reflect your dataset and save it into `datasetDoc.json` file.

```
{
  "about": {
    "datasetID": "xView_dataset",
    "datasetName": "The xView Dataset",
    "datasetSchemaVersion": "4.1.0",
    "datasetVersion": "1.0"
  },
  "dataResources": [
    {
      "resID": "0",
      "resPath": "media/",
      "resType": "image",
      "resFormat": {
        "image/tiff": [
          "tif"
        ]
      },
      "isCollection": true
    },
    {
      "resID": "learningData",
      "resPath": "tables/learningData.csv",
      "resType": "table",
      "resFormat": {
        "text/csv": [
          "csv"
        ]
      },
      "isCollection": false,
      "columns": [
        {
          "colIndex": 0,
          "colName": "d3mIndex",
          "colType": "integer",
          "role": [
            "index"
          ]
        },
        {
          "colIndex": 1,
          "colName": "image_file",
          "colType": "string",
          "role": [
            "attribute"
          ],
          "refersTo": {
            "resID": "0",
            "resObject": "item"
          }
        },
        {
          "colIndex": 2,
          "colName": "timestamp",
          "colType": "dateTime",
          "role": [
            "attribute",
            "timeIndicator"
          ]
        },
        {
          "colIndex": 3,
          "colName": "img_coordinates",
          "colType": "realVector",
          "role": [
            "attribute",
            "boundingPolygon",
            "boundaryIndicator"
          ]
        },
        {
          "colIndex": 4,
          "colName": "geo_coordinates",
          "colType": "realVector",
          "role": [
            "attribute",
            "locationPolygon",
            "locationIndicator"
          ]
        },
        {
          "colIndex": 5,
          "colName": "label",
          "colType": "categorical",
          "role": [
            "suggestedTarget"
          ]
        }
      ]
    }
  ]
}
```  

#### Including GeoTiff Files
If the images are geo tagged (GeoTiff), then the metadata in the `datasetDoc.json` file will change from:

```
      "resFormat": {
        "image/tiff": [
          "tif"
        ]
    }
```

to:

```
      "resFormat": {
        "image/tiff;application=geotiff": [
          "tif"
        ]
    }
``` 

#### Including information about spatial reference system used
If a column contains spatial coordinates, the information about the spatial reference system used for these coordinates can be captured using the field `spatialReferenceSystem` in `datasetDoc.json` as follows:

```
      {
        "colIndex": 4,
        "colName": "geo_coordinates",
        "colType": "realVector",
        "role": [
          "attribute",
          "locationPolygon",
          "locationIndicator"
        ],
        "spatialReferenceSystem":"Universal Transverse Mercator coordinate system"
      },
```

<a name="case2"></a>

## Case 2: Multiple raster images per data point

Here each data point or training example corresponds to a group of raster images. This situation typically arises when there are multi-spectral images and different bands for the same spatio-temporal region are captured in separate images as shown below:

```
xView
  └── xView_dataset
    └── media
    │   ├── S2A_MSIL2A_20170704T112111_28_77_B01.tif
    │   ├── S2A_MSIL2A_20170704T112111_28_77_B02.tif
    │   ├── S2A_MSIL2A_20170704T112111_28_77_B03.tif
    │   ├── S2A_MSIL2A_20170704T112111_28_77_B04.tif
    │   ├── ...
    │   ├── ...
    │   ├── S2A_MSIL2A_20170704T112111_47_61_B01.tif
    │   ├── S2A_MSIL2A_20170704T112111_47_61_B02.tif
    │   ├── S2A_MSIL2A_20170704T112111_47_61_B03.tif
    │   ├── ...
    │   └── ...
    └── tables
    │   └── learningData.csv
    └── datasetDoc.json
```

The corresponding changes to `learningData.csv` is captured in the table below. The main changes here compared to Case 1 is the addition of a group of images. Each image in a group corresponds to a band and each group corresponds to a data point. The grouping in the table is captured by the `group_id` column. The `d3mIndex` and `label` and other relevant metadata for each group are invariant. The addition of a `band` column should also be noted.


| d3mIndex |                image_file                |             group_id             | band |     timestamp    |                                   coordinates                                   |        label        |
|:--------:|:----------------------------------------:|:--------------------------------:|:----:|:----------------:|:-------------------------------------------------------------------------------:|:-------------------:|
| 1        | S2A_MSIL2A_20170704T112111_28_77_B01.tif | S2A_MSIL2A_20170704T112111_28_77 | 1    | 2017-07-04 11:21 | -8.612715,38.906210,-8.612715,38.916977,-8.598814,38.916977,-8.598814,38.906210 | Broad_leaved_forest |
| 1        | S2A_MSIL2A_20170704T112111_28_77_B02.tif | S2A_MSIL2A_20170704T112111_28_77 | 2    | 2017-07-04 11:21 | -8.612715,38.906210,-8.612715,38.916977,-8.598814,38.916977,-8.598814,38.906210 | Broad_leaved_forest |
| 1        | S2A_MSIL2A_20170704T112111_28_77_B03.tif | S2A_MSIL2A_20170704T112111_28_77 | 3    | 2017-07-04 11:21 | -8.612715,38.906210,-8.612715,38.916977,-8.598814,38.916977,-8.598814,38.906210 | Broad_leaved_forest |
| 1        | S2A_MSIL2A_20170704T112111_28_77_B04.tif | S2A_MSIL2A_20170704T112111_28_77 | 4    | 2017-07-04 11:21 | -8.612715,38.906210,-8.612715,38.916977,-8.598814,38.916977,-8.598814,38.906210 | Broad_leaved_forest |
| ...      | ...                                      | ...                              | ...  | ...              | ...                                                                             | ...                 |
| ...      | ...                                      | ...                              | ...  | ...              | ...                                                                             | ...                 |
| 2        | S2A_MSIL2A_20170704T112111_37_55_B01.tif | S2A_MSIL2A_20170704T112111_37_55 | 1    | 2017-07-04 11:21 | -8.486442,39.143620,-8.486442,39.154371,-8.472475,39.154371,-8.472475,39.143620 | Broad_leaved_forest |
| 2        | S2A_MSIL2A_20170704T112111_37_55_B02.tif | S2A_MSIL2A_20170704T112111_37_55 | 2    | 2017-07-04 11:21 | -8.486442,39.143620,-8.486442,39.154371,-8.472475,39.154371,-8.472475,39.143620 | Broad_leaved_forest |
| 2        | S2A_MSIL2A_20170704T112111_37_55_B03.tif | S2A_MSIL2A_20170704T112111_37_55 | 3    | 2017-07-04 11:21 | -8.486442,39.143620,-8.486442,39.154371,-8.472475,39.154371,-8.472475,39.143620 | Broad_leaved_forest |
| 2        | S2A_MSIL2A_20170704T112111_37_55_B04.tif | S2A_MSIL2A_20170704T112111_37_55 | 4    | 2017-07-04 11:21 | -8.486442,39.143620,-8.486442,39.154371,-8.472475,39.154371,-8.472475,39.143620 | Broad_leaved_forest |
| ...      | ...                                      | ...                              | ...  | ...              | ...                                                                             | ...                 |
| ...      | ...                                      | ...                              | ...  | ...              | ...                                                                             | ...                 |
| 3        | S2A_MSIL2A_20170704T112111_40_49_B01.tif | S2A_MSIL2A_20170704T112111_40_49 | 1    | 2017-07-04 11:21 | -8.444274,39.208306,-8.444274,39.219051,-8.430289,39.219051,-8.430289,39.208306 | Broad_leaved_forest |
| 3        | S2A_MSIL2A_20170704T112111_40_49_B02.tif | S2A_MSIL2A_20170704T112111_40_49 | 2    | 2017-07-04 11:21 | -8.444274,39.208306,-8.444274,39.219051,-8.430289,39.219051,-8.430289,39.208306 | Broad_leaved_forest |
| 3        | S2A_MSIL2A_20170704T112111_40_49_B03.tif | S2A_MSIL2A_20170704T112111_40_49 | 3    | 2017-07-04 11:21 | -8.444274,39.208306,-8.444274,39.219051,-8.430289,39.219051,-8.430289,39.208306 | Broad_leaved_forest |
| 3        | S2A_MSIL2A_20170704T112111_40_49_B04.tif | S2A_MSIL2A_20170704T112111_40_49 | 4    | 2017-07-04 11:21 | -8.444274,39.208306,-8.444274,39.219051,-8.430289,39.219051,-8.430289,39.208306 | Broad_leaved_forest |
| ...      | ...                                      | ...                              | ...  | ...              | ...                                                                             | ...                 |


These changes will also be propagated to the dataset schem in the `datasetDoc.json`. The important changes are the addition of new columns to the resource structure as shown below.

```
...
...
    {
      "resID": "learningData",
      "resPath": "tables/learningData.csv",
      "resType": "table",
      "resFormat": {
        "text/csv": [
          "csv"
        ]
      },
      "isCollection": false,
      "columns": [
        {
          "colIndex": 0,
          "colName": "d3mIndex",
          "colType": "integer",
          "role": [
            "multiIndex"
          ]
        },
        {
          "colIndex": 1,
          "colName": "image_file",
          "colType": "string",
          "role": [
            "attribute"
          ],
          "refersTo": {
            "resID": "0",
            "resObject": "item"
          }
        },
        {
          "colIndex": 2,
          "colName": "group_id",
          "colType": "string",
          "role": [
            "attribute",
            "suggestedGroupingKey"
          ]
        },
        {
          "colIndex": 3,
          "colName": "band",
          "colType": "string",
          "role": [
            "attribute",
            "bandIndicator"
          ]
        },
        {
          "colIndex": 4,
          "colName": "timestamp",
          "colType": "dateTime",
          "role": [
            "attribute",
            "timeIndicator"
          ]
        },
        {
          "colIndex": 5,
          "colName": "img_coordinates",
          "colType": "realVector",
          "role": [
            "attribute",
            "boundingPolygon",
            "boundaryIndicator"
          ]
        },
        {
          "colIndex": 6,
          "colName": "geo_coordinates",
          "colType": "realVector",
          "role": [
            "attribute",
            "locationPolygon",
            "locationIndicator"
          ]
        },
        {
          "colIndex": 7,
          "colName": "label",
          "colType": "categorical",
          "role": [
            "suggestedTarget"
          ]
        }
      ]
    }
```

<a name="step5"></a>

### Step 5: Create a problem schema

Aditionally, you can specify a problem associated with this dataset. To specify a problem, create a new directory called `xView_problem` under the project root as shown below. Then create a problem schema and save it in a JSON file called `problemDoc.json` under the `xView_problem` directory as shown below.

```
xView
  └── xView_dataset
  │   └── media
  │   │   ├── S2A_MSIL2A_20170704T112111_28_77_B01.tif
  │   │   ├── S2A_MSIL2A_20170704T112111_28_77_B02.tif
  │   │   ├── S2A_MSIL2A_20170704T112111_28_77_B03.tif
  │   │   ├── S2A_MSIL2A_20170704T112111_28_77_B04.tif
  │   │   ├── ...
  │   │   ├── ...
  │   │   ├── S2A_MSIL2A_20170704T112111_47_61_B01.tif
  │   │   ├── S2A_MSIL2A_20170704T112111_47_61_B02.tif
  │   │   ├── S2A_MSIL2A_20170704T112111_47_61_B03.tif
  │   │   ├── ...
  │   │   └── ...
  │   └── tables
  │   │   └── learningData.csv
  │   └── datasetDoc.json
  └── xView_problem
      └── problemDoc.json
```

The full problem schema definition can be found in the official [D3M problem schema definition](https://gitlab.com/datadrivendiscovery/data-supply/-/blob/shared/documentation/problemSchema.md) document. Refer to this manual for all the details regarding required fields and their semantics and expected range of values.

But the easiest way to get started is to use the following JSON structure and customize it to reflect your problem and save it in the `problemDoc.json` file. Notice that this schema captures some basic information about the problem such as it's ID, name, and versions. It also captures information about the task (multi-class classification task on remotesensing image data) and has a reference to the dataset part along with the information about the target column. Finally, it also captures information about the performance metric (f1Macro in this case).

```
{
  "about": {
    "problemID": "xView_problem",
    "problemName": "Land use classification problem",
    "problemVersion": "1.0.0",
    "problemSchemaVersion": "4.1.0",
    "taskKeywords": [
      "classification",
      "multiClass",
      "remoteSensing",
      "image"
    ]
  },
  "inputs": {
    "data": [
      {
        "datasetID": "xView_dataset",
        "targets": [
          {
            "targetIndex": 0,
            "resID": "learningData",
            "colIndex": 7,
            "colName": "label"
          }
        ]
      }
    ],
    "performanceMetrics": [
      {
        "metric": "f1Macro"
      }
    ]
  }
}
```